<?php

declare(strict_types=1);

namespace Drupal\fs_integration;

use Drupal\Core\Config\ConfigFactory;

use Drupal\Core\Http\ClientFactory as HttpClientFactory;
use GraphQL\Client as GraphqlClient;
use GraphQL\Util\GuzzleAdapter;

/**
 * A client factory that uses the settings for the site.
 */
class ClientFactory {

  public function __construct(
    private readonly HttpClientFactory $httpClientFactory,
    private readonly ConfigFactory $configFactory,
  ) {}

  /**
   * The factory method here.
   */
  public function fromOptions(): FsGraphqlClient {
    $default_settings = [
      'endpoint' => 'https://api.fellesstudentsystem.no/graphql/',
      'username' => '',
      'password' => '',
      'beta' => FALSE,
      'experimental' => FALSE,
    ];
    // Merge in what we have in config.
    $settings = $this->configFactory->get('fs_integration.settings')->get('graphql_settings');
    if (!$settings) {
      $settings = [];
    }
    $settings = array_merge($default_settings, $settings);
    $headers = [];
    $feature_flags = [];
    if ($settings['beta']) {
      $feature_flags[] = 'beta';
    }
    if ($settings['experimental']) {
      $feature_flags[] = 'experimental';
    }
    if ($feature_flags) {
      $headers['Feature-Flags'] = implode(',', $feature_flags);
    }
    $client = $this->httpClientFactory->fromOptions([
      'timeout' => 60,
      'auth' => [$settings['username'], $settings['password']],
      'headers' => $headers,
    ]);
    $client = new GraphqlClient($settings['endpoint'], [], [], new GuzzleAdapter($client));
    return new FsGraphqlClient($client);
  }

}
