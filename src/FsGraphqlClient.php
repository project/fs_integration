<?php

declare(strict_types=1);

namespace Drupal\fs_integration;

use Frontkom\GraphqlClient\Query;
use GraphQL\Client;

/**
 * A client for the FS GraphQL API.
 */
class FsGraphqlClient {

  public function __construct(
    private readonly Client $client,
  ) {}

  /**
   * A wrapper for now.
   */
  public function runQuery(Query $query) {
    return $this->client->runQuery($query);
  }

}
