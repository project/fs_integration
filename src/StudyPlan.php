<?php

declare(strict_types=1);

namespace Drupal\fs_integration;

/**
 * A value object.
 */
class StudyPlan {

  /**
   * Data for the study plan.
   *
   * @var \stdClass
   */
  private $data;

  /**
   * The year of the plan.
   *
   * @var int
   */
  private int $year;

  /**
   * The semester code of the plan.
   *
   * Usually this will be HØST or VÅR (autumn or spring). But theoretically it
   * can be other things.
   *
   * @var string
   */
  private string $semesterCode;

  /**
   * Constructor.
   */
  public function __construct(\stdClass $data) {
    $this->data = $data;
  }

  /**
   * Getter.
   */
  public function getData() {
    return $this->data;
  }

  /**
   * Setter.
   */
  public function setYear(int $year) {
    $this->year = $year;
  }

  /**
   * Getter.
   */
  public function getYear() : ?int {
    return $this->year;
  }

  /**
   * Setter.
   */
  public function setSemesterCode(string $semesterCode) {
    $this->semesterCode = $semesterCode;
  }

  /**
   * Getter.
   */
  public function getSemesterCode() : ?string {
    return $this->semesterCode;
  }

  /**
   * Static helper.
   */
  public static function createFromFsData(\stdClass $data) : self {
    $object = new self($data);
    if (!empty($data->node->gjelderKullFra->arstall)) {
      $object->setYear((int) $data->node->gjelderKullFra->arstall);
    }
    if (!empty($data->node->gjelderKullFra->betegnelse->kode)) {
      $object->setSemesterCode($data->node->gjelderKullFra->betegnelse->kode);
    }
    return $object;
  }

}
