<?php

declare(strict_types=1);

namespace Drupal\fs_integration;

use Frontkom\GraphqlClient\Query;
use GraphQL\InlineFragment;

/**
 * A service for the FS GraphQL API.
 */
final class FsGraphqlService implements FsGraphqlServiceInterface {

  /**
   * Constructs a FsGraphqlClient object.
   */
  public function __construct(
    private readonly FsGraphqlClient $client,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function getStudyPlanById(string $id): StudyPlan {
    $query = $this->getStudyPlanQueryForId($id);
    $response = $this->client->runQuery($query);
    return StudyPlan::createFromFsData($response->getData());
  }

  /**
   * {@inheritdoc}
   */
  public function getStudyPlanQueryForId(string $id) : Query {
    return (new Query('node'))
      ->setArguments(['id' => $id])
      ->setSelectionSet([
        'id',
        (new InlineFragment('StudieoppbygningForKull'))
          ->setSelectionSet([
            'id',
            (new Query('gjelderKullFra'))
              ->setSelectionSet([
                'arstall',
                (new Query('betegnelse'))
                  ->setSelectionSet([
                    'kode',
                  ]),
              ]),
            (new Query('alleOppbygningsdeler'))
              ->setSelectionSet([
                'id',
                (new Query('parent'))
                  ->setSelectionSet([
                    'id',
                  ]),
                (new Query('studieoppbygningsdel'))
                  ->setSelectionSet([
                    'kode',
                    'id',
                    (new Query('emner'))
                      ->setSelectionSet([
                        'rekkefolgenummer',
                        (new Query('terminForPlasseringIUtdanningsplan'))
                          ->setSelectionSet([
                            'terminnummerForhandsvalgt',
                            'terminnummerFra',
                            'terminnummerTil',
                          ]),
                        (new Query('emne'))
                          ->setSelectionSet([
                            'kode',
                            (new Query('navnAlleSprak'))
                              ->setSelectionSet([
                                'en',
                                'nb',
                              ]),
                            (new Query('vekting'))
                              ->setSelectionSet([
                                (new Query('emnevekting'))
                                  ->setSelectionSet([
                                    'verdi',
                                    (new Query('vektingstype'))
                                      ->setSelectionSet([
                                        'kode',
                                      ]),
                                  ]),
                              ]),
                          ]),
                        (new Query('emnekategori'))
                          ->setSelectionSet([
                            'kode',
                          ]),
                      ]),
                  ]),
              ]),
          ]),
      ]);
  }

}
