<?php

declare(strict_types=1);

namespace Drupal\fs_integration;

use Frontkom\GraphqlClient\Query;

/**
 * Interface for the FS GraphQL service.
 */
interface FsGraphqlServiceInterface {

  /**
   * Get a study plan by its id.
   */
  public function getStudyPlanById(string $id): StudyPlan;

  /**
   * Get a query for a study plan by its id.
   */
  public function getStudyPlanQueryForId(string $id) : Query;

}
