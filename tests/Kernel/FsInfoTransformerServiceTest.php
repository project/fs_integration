<?php

namespace Drupal\Tests\fs_integration\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Frontkom\FsInfoTransformer\Transformer;

/**
 * Tests that the transformer service works.
 *
 * @group fs_integration
 */
class FsInfoTransformerServiceTest extends KernelTestBase {

  /**
   * The service to test.
   *
   * @var \Frontkom\FsInfoTransformer\Transformer
   */
  protected Transformer $service;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'fs_integration',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->service = $this->container->get('fs_integration.info_transformer');
  }

  /**
   * Test that the service works.
   *
   * We will reuse the same tests as the library.
   *
   * @dataProvider serviceTestProvider
   */
  public function testServiceWorks($input, $expected) : void {
    $actual = $this->service->transform($input);
    $this->assertEquals($expected, $actual);
  }

  /**
   * Provides test cases for the service.
   */
  public static function serviceTestProvider() : array {
    // Ideally we could have just called that static function here. But it's
    // autoload-dev, as it should. So let's just duplicate the code here.
    $path = DRUPAL_ROOT . '/../vendor/frontkom/fs-info-transformer/tests/assets/cases.json';
    $cases = json_decode(file_get_contents($path), TRUE);
    return array_map(function ($case) {
      return $case;
    }, $cases);
  }

}
