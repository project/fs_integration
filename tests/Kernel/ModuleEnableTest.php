<?php

namespace Drupal\Tests\fs_integration\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests that this module can be enabled.
 *
 * @group fs_integration
 */
class ModuleEnableTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'fs_integration',
  ];

  /**
   * Test that the module can be enabled.
   */
  public function testModuleEnabled() {
    // The fact that it is enabled is implicit. So we just need to test that our
    // code reaches this point, by asserting that true is true.
    $this->assertTrue(TRUE);
  }

}
